﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Techniqly.SportsStore.Common.Entities;

namespace Techniqly.SportsStore.Common.Interfaces
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetAllProductsAsync();
    }
}