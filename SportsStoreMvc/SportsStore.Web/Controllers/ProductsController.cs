﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Techniqly.SportsStore.Common.Interfaces;
using Techniqly.SportsStore.Web.Models;

namespace Techniqly.SportsStore.Web.Controllers
{
    public class ProductsController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public ProductsController(
            IMapper mapper,
            IProductRepository productRepository) : base(mapper)
        {
            _productRepository = productRepository;
        }

        public async Task<ActionResult> Index()
        {
            var products = await _productRepository.GetAllProductsAsync();
            var productViewModels = Mapper.Map<IEnumerable<ProductViewModel>>(products);
            return View(productViewModels);
        }
    }
}