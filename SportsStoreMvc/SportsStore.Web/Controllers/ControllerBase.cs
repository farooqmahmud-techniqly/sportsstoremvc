﻿using System.Web.Mvc;
using AutoMapper;

namespace Techniqly.SportsStore.Web.Controllers
{
    public class ControllerBase : Controller
    {
        protected IMapper Mapper { get; private set; }

        public ControllerBase(IMapper mapper)
        {
            Mapper = mapper;
        }
    }
}