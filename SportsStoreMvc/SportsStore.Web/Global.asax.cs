﻿using System.Web.Mvc;
using System.Web.Routing;
using Autofac.Integration.Mvc;
using Techniqly.SportsStore.Web.Autofac;

namespace Techniqly.SportsStore.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var container = ContainerFactory.Create();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
