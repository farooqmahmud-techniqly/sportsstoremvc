﻿using AutoMapper;
using Techniqly.SportsStore.Common.Entities;

namespace Techniqly.SportsStore.Web.Models.Mappers
{
    public sealed class DomainModelToViewModelMappingProfile : Profile
    {
        public DomainModelToViewModelMappingProfile() : base("DomainModelToViewModelMappingProfile")
        {
            CreateMap<Product, ProductViewModel>();
        }
    }
}