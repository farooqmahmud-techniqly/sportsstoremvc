﻿using System.Reflection;
using Autofac;

namespace Techniqly.SportsStore.Web.Autofac
{
    public sealed class ContainerFactory
    {
        public static IContainer Create()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());
            return builder.Build();
        }
    }
}