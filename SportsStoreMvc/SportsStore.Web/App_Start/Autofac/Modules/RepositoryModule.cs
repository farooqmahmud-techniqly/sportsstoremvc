﻿using Autofac;
using Techniqly.SportsStore.Common.Interfaces;
using Techniqly.SportsStore.DataProvider;

namespace Techniqly.SportsStore.Web.Autofac.Modules
{
    public sealed class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ProductRepository>().As<IProductRepository>().InstancePerRequest();
        }
    }
}