﻿using Autofac;
using Autofac.Integration.Mvc;

namespace Techniqly.SportsStore.Web.Autofac.Modules
{
    public sealed class MvcModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
        }
    }
}