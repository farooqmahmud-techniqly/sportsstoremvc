﻿using System.Collections.Generic;
using System.Linq;
using Autofac;
using ServiceStack.Redis;
using Techniqly.SportsStore.Common.Entities;

namespace Techniqly.SportsStore.Web.Autofac.Modules
{
    public sealed class CacheModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var redisManager = new PooledRedisClientManager(
                new[] {"127.0.0.1:6379"},
                null,
                0);

            var redisClient = redisManager.GetClient();
            builder.RegisterInstance(redisClient).As<IRedisClient>().SingleInstance();

            SeedProducts(redisClient);
        }

        private void SeedProducts(IRedisClient redisClient)
        {
            var redisProductClient = redisClient.As<Product>();
            
            var products = new List<Product>
            {
                new Product {Name = "Football", Price = 25},
                new Product {Name = "Surf board", Price = 179},
                new Product {Name = "Running shoes", Price = 95}
            };

            foreach (var product in products)
            {
                product.ProductId = redisProductClient.GetNextSequence();
                redisProductClient.Store(product);
            }

        }
    }
}