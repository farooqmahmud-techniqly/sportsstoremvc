﻿using System.Collections.Generic;
using Autofac;
using AutoMapper;

namespace Techniqly.SportsStore.Web.Autofac.Modules
{
    public sealed class AutomapperModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(AutomapperModule).Assembly).As<Profile>();

            builder.Register(context => new MapperConfiguration(configuration =>
            {
                foreach (var profile in context.Resolve<IEnumerable<Profile>>())
                {
                    configuration.AddProfile(profile);
                }
            }))
            .AsSelf()
            .SingleInstance();

            builder.Register(context => context.Resolve<MapperConfiguration>()
                .CreateMapper(context.Resolve))
                .As<IMapper>()
                .SingleInstance();
        }
    }
}