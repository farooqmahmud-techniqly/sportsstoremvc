﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ServiceStack.Redis;
using ServiceStack.Redis.Generic;
using Techniqly.SportsStore.Common.Entities;
using Techniqly.SportsStore.Common.Interfaces;

namespace Techniqly.SportsStore.DataProvider
{
    public sealed class ProductRepository : RepositoryBase, IProductRepository
    {
        private readonly IRedisTypedClient<Product> _redisProductClient;

        public ProductRepository(IRedisClient redisClient) : base(redisClient)
        {
             _redisProductClient = redisClient.As<Product>();
        }

        public async Task<IEnumerable<Product>> GetAllProductsAsync()
        {
            var products = _redisProductClient.GetAll().ToList();
            return products;
        }
    }
}