﻿using ServiceStack.Redis;

namespace Techniqly.SportsStore.DataProvider
{
    public abstract class RepositoryBase
    {
        protected IRedisClient RedisClient { get; private set; }

        protected RepositoryBase(IRedisClient redisClient)
        {
            RedisClient = redisClient;
        }
    }
}